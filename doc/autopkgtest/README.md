# pkg-js-autopkgtest

autodep8 includes Node.js files to build autopkgtest files on-the-fly.

## What maintainer has to do

 * in debian/control, insert "Testsuite: autopkgtest-pkg-nodejs"
 * write upstream test in debian/tests/pkg-js/test (will be launched by
   sh)
 * if some other files than "test\*" and installed files are needed,
   write a "debian/tests/pkg-js/files" with all needed files

That's all, other debian/tests files will be written on-the-fly by
autodep8 during autopkgtest

If you want to launch the same test during build, add this in debian/rules:
```
%:
        dh $@ --with nodejs
```
and add "pkg-js-tools" in build dependencies

## How it works

 * autopkgtest will launch 2 tests:
   - a `node -e "require('name')`
   - the test defined in debian/tests/pkg-js/test in a temporary dir (it
     links installed files)
 * if `dh $@ --with nodejs` is set in `debian/rules`, dh_auto_test will launch
   the same test _(debian/tests/pkg-js/test)_ if exists, else just a
   `node -e "require('.')"`

## Full example

* debian/control

```
...
Testsuite: autopkgtest-pkg-nodejs
Build-Depends: pkg-js-tools
...
```

* debian/rules

```
#!/usr/bin/make -f
# -*- makefile -*-

%:
	dh --with nodejs
```

* debian/tests/pkg-js/test

```shell
mocha -R spec
```

