# pkg-js/debhelper -- lintian check script for checking Vcs-* headers -*- js -*-
#
# Copyright © 2013 Niels Thykier <niels@thykier.net>
# Copyright © 2013 gregor herrmann <gregoa@debian.org>
# Copyright © 2013 Axel Beckert <abe@debian.org>
# Copyright © 2019 Xavier Guimard <yadd@debian.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, you can find it on the World Wide
# Web at http://www.gnu.org/copyleft/gpl.html, or write to the Free
# Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,
# MA 02110-1301, USA.

package Lintian::pkg_js::vcs;

use strict;
use warnings;

use Moo;
use namespace::clean;

with 'Lintian::Check';

sub source {
    my ( $self ) = @_;

    my $processable = $self->processable;

    # Only for pkg-js packages
    return
      unless $processable->field('maintainer') =~
      /pkg-javascript-maintainers\@lists\.alioth\.debian\.org/sm;

    # Check if the Vcs headers are git
    my @non_git_vcs_fields =
      qw(vcs-arch vcs-bzr vcs-cvs vcs-darcs vcs-hg vcs-mtn vcs-svn);
    foreach my $field_name (@non_git_vcs_fields) {
        my $field_value = $processable->field($field_name);
        if ( defined($field_value) ) {
            $self->tag('pkg-js-vcs-not-git', "$field_name");
        }
    }

    # Check if repo is writable for the Debian Perl Group
    foreach my $field_name ( @non_git_vcs_fields, qw(vcs-git vcs-browser) ) {
        my $field_value = $processable->field($field_name);
        if ( defined($field_value)
            && $field_value !~ m{^https://salsa.debian.org/js-team}i )
        {
            $self->tag('vcs-url-not-under-js-team', "$field_value");
        }
    }
    return;
}

1;
