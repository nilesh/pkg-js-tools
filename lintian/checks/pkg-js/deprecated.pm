package Lintian::pkg_js::deprecated;

use strict;
use warnings;

use Moo;
use namespace::clean;

with 'Lintian::Check';

sub files {
    my ($self, $file) = @_;
    return unless $file->is_file;
    return unless $file->name =~ /\.js$/;
    return unless $file->is_open_ok;
    my $content = $file->file_contents;
    $self->tag('nodejs-bad-buffer-usage') if $content =~ /\bnew\s+Buffer\(/;
}

1;
