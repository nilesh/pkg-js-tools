# pkg-js/no-testsuite -- lintian check script for detecting a missing Testsuite header -*- js -*-
#
# Copyright © 2013 Niels Thykier <niels@thykier.net>
# Copyright © 2013 gregor herrmann <gregoa@debian.org>
# Copyright © 2014 Niko Tyni <ntyni@debian.org>
# Copyright © 2018 Florian Schlichting <fsfs@debian.org>
# Copyright © 2019 Xavier Guimard <yadd@debian.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, you can find it on the World Wide
# Web at http://www.gnu.org/copyleft/gpl.html, or write to the Free
# Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,
# MA 02110-1301, USA.

package Lintian::pkg_js::testsuite;

use strict;
use warnings;

use Moo;
use namespace::clean;

with 'Lintian::Check';

sub source {
    my ( $self ) = @_;

    my $processable = $self->processable;

    # Only for pkg-js packages
    return
      unless $processable->field('maintainer') =~
      /pkg-javascript-maintainers\@lists\.alioth\.debian\.org/sm;

    if ( !defined $processable->field('testsuite') ) {
        $self->tag('missing-pkg-js-testsuite-header');
        return;
    }

    if ( $processable->field('testsuite') ne 'autopkgtest-pkg-nodejs' ) {
        $self->tag('non-js-team-testsuite-header', $processable->field('testsuite'));
        return;
    }
    return;
}

1;
